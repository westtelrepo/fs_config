# notes for dev

You'll want to install rust (through rustup or similar)

Might be useful for continuous integration/etc: `cargo install cargo-audit cargo-deny cargo-edit cargo-geiger cargo-outdated cargo-tree`

Ideally everything should pass `cargo clippy` (CI should block on this) (and `cargo check`)

NOTE: because of libxml dependency, the best way to (production) compile this is to compile on an old version of Debian. (TODO: CI/Jenkins)