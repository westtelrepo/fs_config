* TEST: fs_config upgrade-fs-from directory (/opt/freeswitch-*versionstringinfo*)
* http://wiki.lan/display/EK/Set+up+FreeSWITCH+for+Master-Slave+HA add HA awareness
* update dependencies
** remove structopt, merged into clap
* use anyhow instead of panics everywhere
* cargo-deny?

* write an actual "FreeSWITCH XML" parser that understands and can format FreeSWITCH's non-standard dialect of XML
NOTES:
* has meaningful comments (<!--#set x=v -->, also <X-PRE-PROCESS> executes when inside a comment)
* has *executable* stuff (aka X-PRE-PROCESS can run shell commands)
* ignores any line that contains "<?" or "<include>" or "</include>" (etc, look at FS source)
* $${var} is expanded very early in parsing time (even expands in comments!)
* is very generous in what it accepts
* VERIFY THIS: doesn't use/understand text nodes and/or CDATA?
* probably doesn't understand namespaces