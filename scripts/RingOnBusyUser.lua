#!/usr/bin/lua

freeswitch.consoleLog("info","RingOnBusyUser v1.0\n")
-- Used to dial Users (not shared lines)
-- Will keep ringing even if user is busy
-- usage in dialplan: 
-- 
-- <action application="lua" data="RingOnBusyUser.lua ${destination_number}"/>
--
--Version 1.0

function callTry(number)
	
    DOMAIN = freeswitch.getGlobalVariable("domain");
       
	freeswitch.consoleLog("notice", "*********** DIALING NUMBER! ***********\n");
        if (session:ready()) then
          session:execute("ring_ready");
          freeswitch.consoleLog("notice", "*********** RINGBACK! ***********\n");
	    end
	
        new_session = freeswitch.Session("user/"..number.."@"..DOMAIN, session);

        local cause = new_session:hangupCause();
	
	freeswitch.consoleLog("notice", "*********** CAUSE: "..cause.." ***********\n"); 
	
	if(cause == "USER_BUSY") then
		freeswitch.consoleLog("notice", "*********** HANGING UP ***********\n"); 
		session:sleep(1000);
		freeswitch.consoleLog("notice", "*********** TRYING AGAIN ***********\n"); 
		return true;
	end
		
--      IF everybody is ready, then bridge our current session & the new_session
	if (new_session:ready()) then
	   freeswitch.bridge(session, new_session);
	end
	return false;
end
-- ************************ Main **************************************************
numberdialed = argv[1]

while (callTry(numberdialed)) do

end

-- hangup when done
if (new_session:ready()) then
 new_session.hangup();
end;
if (session:ready()) then
 session.hangup();
end;
