#!/usr/bin/lua
io.write("hangup hook ......................\r\n");


obCause = session:getVariable("originate_disposition")



dest_number     = env:getHeader("Caller-Destination-Number")
obCause         = env:getHeader("hangup_cause")
XferFrom        = env:getHeader("transfer_from_position")
ControllerId    = env:getHeader("controller_uniqueid")


freeswitch.consoleLog("INFO","Execute Hangup Hook Transfer to Number: "..dest_number..", Hangup Cause: "..obCause.."\n")

if ( obCause == "USER_BUSY" ) then 
	session:execute("playback", "ivr/8000/ivr-user_busy.wav");    
	session:execute("playback", "ivr/8000/ivr-please_try_again.wav");    
	session:execute("playback", "tone_stream://%(500,500,480,620);loops=5");    
elseif ( obCause == "NO_ANSWER" ) then
	session:execute("playback", "ivr/8000/ivr-im_sorry.wav");    
	session:execute("playback", "misc/8000/misc-your_call_has_been_terminated.wav");    
elseif ( obCause == "ORIGINATOR_CANCEL" ) then 
       -- do nothing
elseif ( obCause == "NO_ROUTE_TRANSIT_NET" ) then
	session:execute("playback", "ivr/8000/ivr-im_sorry.wav");    
	session:execute("playback", "ivr/8000/ivr-no_route_destination.wav"); 
elseif ( obCause == "NO_ROUTE_DESTINATION" ) then 
	session:execute("playback", "ivr/8000/ivr-im_sorry.wav");    
	session:execute("playback", "ivr/8000/ivr-no_route_destination.wav"); 
elseif ( obCause == "CHANNEL_UNACCEPTABLE" ) then
	session:execute("playback", "misc/8000/error.wav");    
elseif ( obCause == "NO_USER_RESPONSE" ) then   
	session:execute("playback", "ivr/8000/ivr-no_user_response.wav");    
elseif ( obCause == "SUBSCRIBER_ABSENT" ) then  
	session:execute("playback", "ivr/8000/ivr-im_sorry.wav");
	session:execute("playback", "ivr/8000/ivr-call_cannot_be_completed_as_dialed.wav");    
elseif ( obCause == "UNALLOCATED_NUMBER" ) then  
	session:execute("playback", "ivr/8000/ivr-call_cannot_be_completed_as_dialed.wav");
	session:execute("playback", "ivr/8000/ivr-unallocated_number.wav");
elseif ( obCause == "CALL_REJECTED" ) then   
	session:execute("playback", "ivr/8000/ivr-call_rejected.wav");
elseif ( obCause == "NUMBER_CHANGED" ) then 
	session:execute("playback", "ivr/8000/ivr-im_sorry.wav");
	session:execute("playback", "ivr/8000/ivr-call_cannot_be_completed_as_dialed.wav"); 
	session:execute("playback", "ivr/8000/ivr-please_check_number_try_again.wav"); 
elseif ( obCause == "EXCHANGE_ROUTING_ERROR" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "DESTINATION_OUT_OF_ORDER" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "INVALID_NUMBER_FORMAT" ) then 
	session:execute("playback", "ivr/8000/ivr-im_sorry.wav");
	session:execute("playback", "ivr/8000/ivr-call_cannot_be_completed_as_dialed.wav"); 
	session:execute("playback", "ivr/8000/ivr-please_check_number_try_again.wav"); 
elseif ( obCause == "FACILITY_REJECTED" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "RESPONSE_TO_STATUS_ENQUIRY" ) then  
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "NORMAL_CIRCUIT_CONGESTION" ) then   
	session:execute("playback", "tone_stream://%(500,500,480,620);loops=5");    
elseif ( obCause == "NETWORK_OUT_OF_ORDER" ) then  
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "NORMAL_TEMPORARY_FAILURE" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "SWITCH_CONGESTION" ) then 
	session:execute("playback", "tone_stream://%(500,500,480,620);loops=5");    
elseif ( obCause == "ACCESS_INFO_DISCARDED" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "REQUESTED_CHAN_UNAVAIL" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "PRE_EMPTED" ) then 
	session:execute("playback", "misc/8000/misc-your_call_has_been_terminated.wav"); 
elseif ( obCause == "FACILITY_NOT_SUBSCRIBED" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "OUTGOING_CALL_BARRED" ) then 
	session:execute("playback", "ivr/8000/ivr-not_have_permission.wav"); 
elseif ( obCause == "SERVICE_NOT_IMPLEMENTED" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "CHAN_NOT_IMPLEMENTED" ) then 
	session:execute("playback", "misc/8000/error.wav");
elseif ( obCause == "INVALID_CALL_REFERENCE" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "INCOMPATIBLE_DESTINATION" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "RECOVERY_ON_TIMER_EXPIRE" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "PROTOCOL_ERROR" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "INTERWORKING" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "CRASH" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "SYSTEM_SHUTDOWN" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "ALLOTTED_TIMEOUT" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "USER_CHALLENGE" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "MEDIA_TIMEOUT" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "PICKED_OFF" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "USER_NOT_REGISTERED" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "PROGRESS_TIMEOUT" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "INVALID_GATEWAY" ) then 
	session:execute("playback", "misc/8000/error.wav");
elseif ( obCause == "GATEWAY_DOWN" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
elseif ( obCause == "INVALID_PROFILE" ) then 
	session:execute("playback", "misc/8000/error.wav"); 
else
	session:execute("playback", "ivr/8000/ivr-call_cannot_be_completed_as_dialed.wav"); 
end





