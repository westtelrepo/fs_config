#!/usr/bin/lua
freeswitch.consoleLog("info","Record Session v1.2 \n")

--v1.1 Set "RECORD_STEREO=false
--v1.2 reduce sleep times from 500 to 50

uuid = argv[1]

api = freeswitch.API();
local newUUID="Stream_UUID="..api:executeString("create_uuid");
session:execute("set", newUUID);

local socket=require'socket'

data="enable_file_write_buffering=false"
session:execute("export",data);

data="recording_follow_transfer=true"
session:execute("export",data);

data="RECORD_STEREO=false"
session:execute("export",data);


recording_dir  = session:getVariable("recording_dir");
streamuuid      = session:getVariable("Stream_UUID");

data      = "v1_"..socket.gettime().."_"..uuid.."_"..streamuuid..".wav"

sipheader ="sip_h_X-Recording="..data;
session:execute("set",sipheader);
--session:execute("export",sipheader);

session:execute("record_session",recording_dir.."/"..data);


session:sleep(50);

--fire event to event socket
local event = freeswitch.Event("CUSTOM", "myevent::message");
event:addHeader("My-Name", "IRR_RECORDING_FILE_LOCATION");
event:addHeader("controller_uniqueid", uuid);
event:addHeader("recording_file_location", data);
-- file location is a misnomer it is actually file name 
event:fire();

session:sleep(50);


return

