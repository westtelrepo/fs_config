#!/bin/bash
# version 1.0 11/29/2017
#
#
if [[ $EUID -ne 0 ]]; then
        printf 'You must run this as root\n'
        exit 1
fi

# exit on error
set -e

# Note: iptables-persistent must be installed
if [ $(dpkg-query -W -f='${Status}' iptables-persistent 2>/dev/null | grep -c "ok installed") -eq 0 ]
then
DEBIAN_FRONTEND=noninteractive apt-get -yq install iptables-persistent || echo "iptables-persistent Installation failed" && exit
fi


#found in file /etc/freeswitch/autoload_configs/switch.xml
rtpportstart=10000
rtpportend=20000

#Clear all rules ... IP4
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
iptables -t nat -F
iptables -t mangle -F
iptables -F
iptables -X

#Clear all rules ... IP6
ip6tables -P INPUT ACCEPT
ip6tables -P FORWARD ACCEPT
ip6tables -P OUTPUT ACCEPT
ip6tables -t nat -F
ip6tables -t mangle -F
ip6tables -F
ip6tables -X

# Mark RTP packets with EF:
iptables -t mangle -A OUTPUT -p udp -m udp --sport $rtpportstart:$rtpportend -j DSCP --set-dscp-class ef

# Mark SIP UDP packets with CS3:
iptables -t mangle -A OUTPUT -p udp -m udp --sport 5060 -j DSCP --set-dscp-class cs3
iptables -t mangle -A OUTPUT -p udp -m udp --dport 5060 -j DSCP --set-dscp-class cs3

# Mark SIP TCP packets with CS3:
iptables -t mangle -A OUTPUT -p tcp --sport 5060 -j DSCP --set-dscp-class cs3
iptables -t mangle -A OUTPUT -p tcp --dport 5060 -j DSCP --set-dscp-class cs3

# Mark SIP TLS packets with CS3:
iptables -t mangle -A OUTPUT -p tcp --sport 5061 -j DSCP --set-dscp-class cs3
iptables -t mangle -A OUTPUT -p tcp --dport 5061 -j DSCP --set-dscp-class cs3

dpkg-reconfigure iptables-persistent -f noninteractive







