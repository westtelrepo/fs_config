#![forbid(unsafe_code)]

#[macro_use]
extern crate lazy_static;

use ansi_term::Colour;
use duct::cmd;
use libxml::parser::{ParseFormat, Parser};
use libxml::tree::document::SaveOptions;
use libxml::tree::{Document, Node, NodeType};
use prettydiff::diff_lines;
use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::env;
use std::ffi::OsStr;
use std::fs;
use std::io;
use std::io::prelude::*;
use std::os::unix::fs::MetadataExt;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use std::str::from_utf8;
use std::thread::sleep;
use std::time::Duration;
use structopt::StructOpt;
use users::get_current_uid;
use walkdir::WalkDir;

// TODO: update default_odbc_dsn in vars.xml?

#[derive(Debug, StructOpt)]
/// WestTel FreeSWITCH Configurator
struct Opt {
    #[structopt(long, env, default_value = "/etc/freeswitch")]
    freeswitch_config_dir: PathBuf,
    #[structopt(subcommand)]
    cmd: OptCommand,
}

#[derive(Debug, StructOpt)]
enum OptCommand {
    /// update the configuration in the staging directory
    UpdateConfiguration,
    /// commit the configuration in the staging directory
    Commit,
    /// create the staging directory (/etc/freeswitch.staging) from production (/etc/freeswitch)
    Stage,
    /// show a diff between staging and production config directories
    Diff,
    /// run some linters on staging directory
    LintStaging,
    /// run some linters on production directory
    LintProduction,
    /// run ldd on files in directory
    LddCheck {
        /// directory to check
        directory: String,
    },
    /// WARNING NOT DONE: do the actual FreeSWITCH upgrade
    UpgradeFS {
        /// directory in /opt where new FreeSWITCH version is held
        directory: String,
    },
    /// installs and/or updates scripts
    UpdateScripts {
        #[structopt(
            default_value = "/usr/local/share/freeswitch/scripts",
            env = "FREESWITCH_SCRIPTS_DIR"
        )]
        /// FreeSWITCH scripts directory
        directory: String,
    },
    /// shows what `update-scripts` would do
    DiffScripts {
        #[structopt(
            default_value = "/usr/local/share/freeswitch/scripts",
            env = "FREESWITCH_SCRIPTS_DIR"
        )]
        /// FreeSWITCH scripts directory
        directory: String,
    },
}

fn main() {
    let opt = Opt::from_args();
    let staging_dir = opt.freeswitch_config_dir.with_extension("staging");

    match opt.cmd {
        OptCommand::UpdateConfiguration => {
            if !staging_dir.exists() {
                eprintln!("refusing to update configuration without staging directory, run `fs_config stage` first");
            } else {
                overwrite_db_setting_for_configuration(
                    staging_dir
                        .join("autoload_configs/db.conf.xml")
                        .to_str()
                        .unwrap(),
                    "db.conf",
                    "odbc-dsn",
                );
                overwrite_db_setting_for_configuration(
                    staging_dir
                        .join("autoload_configs/fifo.conf.xml")
                        .to_str()
                        .unwrap(),
                    "fifo.conf",
                    "odbc-dsn",
                );
                overwrite_db_setting_for_configuration(
                    staging_dir
                        .join("autoload_configs/switch.conf.xml")
                        .to_str()
                        .unwrap(),
                    "switch.conf",
                    "core-db-dsn",
                );
                overwrite_db_setting_for_profile(
                    staging_dir
                        .join("sip_profiles/internal.xml")
                        .to_str()
                        .unwrap(),
                    "internal",
                );
                overwrite_db_setting_for_profile(
                    staging_dir
                        .join("sip_profiles/internal-ipv6.xml")
                        .to_str()
                        .unwrap(),
                    "internal-ipv6",
                );
                overwrite_db_setting_for_profile(
                    staging_dir
                        .join("sip_profiles/external.xml")
                        .to_str()
                        .unwrap(),
                    "external",
                );
                overwrite_db_setting_for_profile(
                    staging_dir
                        .join("sip_profiles/external-ipv6.xml")
                        .to_str()
                        .unwrap(),
                    "external-ipv6",
                );
                update_modules(
                    staging_dir
                        .join("autoload_configs/modules.conf.xml")
                        .to_str()
                        .unwrap(),
                );
                update_preload_modules(
                    staging_dir
                        .join("autoload_configs/pre_load_modules.conf.xml")
                        .to_str()
                        .unwrap(),
                );
                update_conference(
                    staging_dir
                        .join("autoload_configs/conference.conf.xml")
                        .to_str()
                        .unwrap(),
                );
                overwrite_fs_configs(&staging_dir);
                remove_unused_configs(&staging_dir);
                update_custom_recordings(staging_dir.join("custom_recordings"));
            }
        }
        OptCommand::Commit => {
            let old_dir = opt.freeswitch_config_dir.with_extension("bak");
            if old_dir.exists() {
                eprintln!(
                    "refusing to delete backup directory {}, cannot commit",
                    old_dir.to_string_lossy()
                );
            } else if !staging_dir.exists() {
                eprintln!(
                    "cannot stage non existant staging directory, did you run `fs_config stage`?"
                );
            } else {
                fs::rename(&opt.freeswitch_config_dir, old_dir).unwrap();
                fs::rename(staging_dir, opt.freeswitch_config_dir).unwrap();
            }
        }
        OptCommand::Stage => {
            if staging_dir.exists() {
                eprintln!(
                    "refusing to override existing staging directory {}, please delete or commit",
                    staging_dir.to_string_lossy()
                );
            } else {
                cmd!("cp", "-a", "--", &opt.freeswitch_config_dir, &staging_dir)
                    .stdin_null()
                    .run()
                    .expect("could not stage");
                println!(
                    "Copied {} to {}",
                    opt.freeswitch_config_dir.to_string_lossy(),
                    staging_dir.to_string_lossy()
                );
            }
        }
        OptCommand::Diff => {
            if !staging_dir.exists() {
                eprintln!("Cannot show diff from current to staging directory if staging directory does not exist");
            } else {
                println!(
                    "diff -r --color --unified -- {} {}",
                    opt.freeswitch_config_dir.to_string_lossy(),
                    staging_dir.to_string_lossy()
                );
                Command::new("diff")
                    .arg("-r")
                    .arg("--color")
                    .arg("--unified")
                    .arg("--")
                    .arg(&opt.freeswitch_config_dir)
                    .arg(&staging_dir)
                    .stdin(Stdio::null())
                    .spawn()
                    .unwrap()
                    .wait()
                    .unwrap();
            }
        }
        OptCommand::LintStaging => {
            if !staging_dir.exists() {
                eprintln!("Cannot lint non-existent staging directory");
            } else {
                check_fs_packages();
                check_systemd_file();
                fs_lint(&staging_dir);
            }
        }
        OptCommand::LintProduction => {
            check_fs_packages();

            // check permissions of /etc/freeswitch
            if get_current_uid() == 0 {
                for file in WalkDir::new(&opt.freeswitch_config_dir)
                    .into_iter()
                    .filter_entry(|entry| {
                        !(entry.file_name().to_str() == Some("tls") && entry.depth() == 1)
                    })
                    .map(|entry| entry.unwrap())
                {
                    let meta = fs::metadata(&file.path()).unwrap();
                    if meta.uid() != 0 || meta.gid() != 0 {
                        eprintln!(
                            "user or group for {} is not root",
                            file.path().to_string_lossy()
                        );
                    }
                }
            }

            // run config linter
            fs_lint(&opt.freeswitch_config_dir);

            check_systemd_file();
        }
        OptCommand::LddCheck { directory } => {
            ldd_check(directory);
        }
        OptCommand::UpgradeFS { directory } => {
            upgrade_fs(directory);
        }
        OptCommand::UpdateScripts { directory } => {
            update_scripts(directory);
        }
        OptCommand::DiffScripts { directory } => {
            diff_scripts(directory);
        }
    }
}

lazy_static! {
    static ref SCRIPTS: HashMap<&'static Path, &'static str> = [
        (
            Path::new("convert_8_digit_ani.lua"),
            include_str!("../scripts/convert_8_digit_ani.lua")
        ),
        (
            Path::new("getlocationURI.lua"),
            include_str!("../scripts/getlocationURI.lua")
        ),
        (
            Path::new("hanguphook.lua"),
            include_str!("../scripts/hanguphook.lua")
        ),
        (
            Path::new("irr_record.lua"),
            include_str!("../scripts/irr_record.lua")
        ),
        (
            Path::new("mem.usage.lua"),
            include_str!("../scripts/mem.usage.lua")
        ),
        (
            Path::new("RingOnBusyUser.lua"),
            include_str!("../scripts/RingOnBusyUser.lua")
        ),
        (
            Path::new("SetTimeStamp.lua"),
            include_str!("../scripts/SetTimeStamp.lua")
        ),
        (
            Path::new("SetTrunk.lua"),
            include_str!("../scripts/SetTrunk.lua")
        ),
        (
            Path::new("test_for_conference.lua"),
            include_str!("../scripts/test_for_conference.lua")
        )
    ]
    .iter()
    .cloned()
    .collect();
}

fn update_scripts<P: AsRef<Path>>(dir: P) {
    let dir = dir.as_ref();

    // create script directory if it doesn't already exist
    fs::create_dir_all(dir)
        .unwrap_or_else(|_| panic!("could not create directory {}", dir.to_string_lossy()));

    let style = Colour::Cyan.underline().bold();

    for (&name, &content) in &*SCRIPTS {
        let file = dir.join(name);
        if file.exists() {
            let existing_file_contents = fs::read_to_string(&file)
                .unwrap_or_else(|_| panic!("could not read {}", file.to_string_lossy()));

            if content == existing_file_contents {
                // do nothing, it's already as expected
            } else {
                println!(
                    "{}",
                    style.paint(format!("diff for {}:", name.to_string_lossy()))
                );
                println!("{}", diff_lines(existing_file_contents.as_ref(), content));

                print!(
                    "{}",
                    style.paint(format!("Update {}? [Y/n] ", name.to_string_lossy()))
                );
                io::stdout().flush().unwrap();

                let mut line = String::new();
                io::stdin().read_line(&mut line).unwrap();

                if line == "Y\n" || line == "y\n" || line == "yes\n" || line == "\n" {
                    println!(
                        "{}",
                        style.paint(format!(
                            "overwriting {} with changes",
                            name.to_string_lossy()
                        ))
                    );
                    fs::write(file.with_extension("lua.bak"), existing_file_contents)
                        .unwrap_or_else(|_| {
                            panic!("could not write backup file for {}", name.to_string_lossy())
                        });
                    fs::write(file, content).unwrap_or_else(|_| {
                        panic!("could not write {}", dir.join(name).to_string_lossy())
                    });
                } else {
                    println!(
                        "{}",
                        style.paint(format!("ignoring {}", name.to_string_lossy()))
                    );
                }
            }
        } else {
            // if the file doesn't exist, we just write
            println!("adding script {}", name.to_string_lossy());
            fs::write(file, content)
                .unwrap_or_else(|_| panic!("could not write {}", dir.join(name).to_string_lossy()));
        }
    }
}

fn diff_scripts<P: AsRef<Path>>(dir: P) {
    let dir = dir.as_ref();

    if !dir.exists() {
        println!(
            "Script directory ({}) does not exist",
            dir.to_string_lossy()
        );
        return;
    }

    let style = Colour::Cyan.underline().bold();

    for (&name, &content) in &*SCRIPTS {
        let file = dir.join(name);
        if file.exists() {
            let existing_file_contents = fs::read_to_string(&file)
                .unwrap_or_else(|_| panic!("could not read {}", file.to_string_lossy()));

            if content == existing_file_contents {
                // do nothing
            } else {
                println!("{}", style.paint(format!("{}:", name.to_string_lossy())));
                println!("{}", diff_lines(existing_file_contents.as_ref(), content));
            }
        } else {
            println!(
                "{}",
                style.paint(format!(
                    "{} does not currently exist",
                    file.to_string_lossy()
                ))
            );
        }
    }
}

fn upgrade_fs<P: AsRef<Path>>(dir: P) {
    let dir = dir.as_ref();

    if !dir.join("bin/freeswitch").exists() {
        eprintln!(
            "Refusing to do upgrade, {} does not appear to be a freeswitch directory",
            dir.to_string_lossy()
        );
        return;
    }

    if Path::new("/opt/freeswitch.bak").exists() {
        eprintln!(
            "Refusing to do upgrade, /opt/freeswitch.bak already exists, either move or delete"
        );
        return;
    }

    if !dir.starts_with("/opt") {
        eprintln!(
            "Refusing to do upgrade, {} is not in /opt",
            dir.to_string_lossy()
        );
        return;
    }

    if Path::new("/opt/freeswitch").exists()
        && !Path::new("/opt/freeswitch/bin/freeswitch").exists()
    {
        eprintln!("Refusing to do upgrade, /opt/freeswitch exists but does not appear to be a FreeSWITCH directory");
        return;
    }

    if !Path::new("/opt/freeswitch").exists() {
        eprintln!("Refusing to do upgrade, /opt/freeswitch does not exist");
        return;
    }

    println!("shutting down FreeSWITCH elegantly...");

    // if this fails:
    // * FreeSWITCH may not be currently running
    // * some other failure
    cmd!("fs_cli", "-T", "500", "-x", "fsctl shutdown elegant")
        .stdin_null()
        .run()
        .expect("could not run fs_cli");

    sleep(Duration::from_millis(100));

    cmd!("systemctl", "stop", "freeswitch.service")
        .stdin_null()
        .run()
        .expect("could not run `systemctl stop freeswitch.service`");

    println!("Backing up /opt/freeswitch to /opt/freeswitch.bak...");

    fs::rename(
        Path::new("/opt/freeswitch"),
        Path::new("/opt/freeswitch.bak"),
    )
    .expect("could not rename /opt/freeswitch to /opt/freeswitch.bak");

    println!("Renaming {} to /opt/freeswitch...", dir.to_string_lossy());

    fs::rename(dir, Path::new("/opt/freeswitch")).unwrap_or_else(|_| {
        panic!(
            "could not rename {} to /opt/freeswitch",
            dir.to_string_lossy()
        )
    });

    println!("Starting up FreeSWITCH...");

    // TODO/maybe: put back old version and start it if this fails?
    cmd!("systemctl", "start", "freeswitch.service")
        .stdin_null()
        .run()
        .expect("did not start FreeSWITCH successfully");
}

fn check_systemd_file() {
    // check for -nonat in service file
    let freeswitch = cmd!("systemctl", "cat", "freeswitch.service")
        .stderr_to_stdout()
        .stdout_capture()
        .unchecked()
        .run()
        .expect("could not run systemctl cat freeswitch");
    if !freeswitch.status.success() {
        println!(
            "Could not read freeswitch service file (does it exist?): {}",
            from_utf8(&freeswitch.stdout).unwrap()
        );
    }
    let service_file = from_utf8(&freeswitch.stdout).unwrap();
    if service_file
        .lines()
        .any(|s| s.starts_with("ExecStart=/opt/freeswitch/bin/freeswitch") && s.contains("-nonat"))
    {
        println!("FreeSWITCH service file appears to have -nonat in it, please remove. You will likely edit this file with `sudo systemctl edit --full freeswitch`");
    }

    if !service_file.lines().any(|s| s == "Restart=on-failure") {
        println!("FreeSWITCH service file does not appear to have `Restart=on-failure` in it, please fix. Edit with `sudo systemctl edit --full freeswitch`");
    }
}

fn ldd_check<P: AsRef<Path>>(directory: P) {
    let dir = directory.as_ref();

    let freeswitch = dir.join("bin/freeswitch");

    if !freeswitch.exists() {
        println!(
            "invalid directory passed to ldd-check, {} does not exist",
            freeswitch.to_string_lossy()
        );
    } else {
        let mut files_to_check = vec![
            freeswitch,
            dir.join("bin/fs_cli"),
            dir.join("bin/fs_encode"),
            dir.join("bin/fs_ivrd"),
            dir.join("bin/fs_tts"),
            dir.join("bin/tone2wav"),
            dir.join("lib/libfreeswitch.so.1.0.0"),
        ];

        // TODO: read configuration, only check modules that are actually loaded
        for file in WalkDir::new(dir.join("lib/freeswitch/mod"))
            .into_iter()
            .map(|entry| entry.unwrap())
            .filter(|entry| entry.file_type().is_file())
            .filter(|entry| entry.path().to_string_lossy().ends_with(".so"))
        {
            files_to_check.push(file.path().to_owned());
        }

        let mut was_error = false;
        for file in files_to_check {
            let m = ldd(&file, dir.join("lib"));
            for (library, location) in m {
                if location == None {
                    println!(
                        "Executable/shared object {} needs to load {} but can not find it (is the appropriate package installed?)",
                        file.to_string_lossy(),
                        library
                    );
                    was_error = true;
                }
            }
        }

        if !was_error {
            println!("no errors found");
        }
    }
}

/// Returns library name -> loaded path.
/// If loaded path is `None` it wasn't found (ldd said "not found").
/// If there was no loaded path (for example, linux-vdso.so.1) loaded path will be "".
fn ldd<P: AsRef<Path>, Q: AsRef<Path>>(
    file: P,
    prefix_ld_path: Q,
) -> HashMap<String, Option<String>> {
    let f = file.as_ref();

    let set_ld_library_path = match std::env::var("LD_LIBRARY_PATH") {
        Ok(val) => format!("{}:{}", prefix_ld_path.as_ref().to_str().unwrap(), val),
        Err(e) => match e {
            env::VarError::NotPresent => prefix_ld_path.as_ref().to_str().unwrap().to_owned(),
            env::VarError::NotUnicode(_) => panic!("LD_LIBRARY_PATH is not valid unicode"),
        },
    };

    let result = cmd!("ldd", f)
        .env("LD_LIBRARY_PATH", set_ld_library_path)
        .read()
        .unwrap_or_else(|_| panic!("could not run ldd on {}", f.to_string_lossy()));

    let mut ret: HashMap<String, Option<String>> = HashMap::new();

    let re1 = Regex::new(r#"^\t(\S+) \(\S+\)$"#).unwrap();
    let re2 = Regex::new(r#"^\t(\S+) => (\S+) \(\S+\)$"#).unwrap();
    let re3 = Regex::new(r#"^\t(\S+) => not found$"#).unwrap();
    for line in result.lines() {
        if let Some(mat) = re1.captures(line) {
            ret.insert(mat.get(1).unwrap().as_str().to_owned(), Some("".to_owned()));
        } else if let Some(mat) = re2.captures(line) {
            ret.insert(
                mat.get(1).unwrap().as_str().to_owned(),
                Some(mat.get(2).unwrap().as_str().to_owned()),
            );
        } else if let Some(mat) = re3.captures(line) {
            ret.insert(mat.get(1).unwrap().as_str().to_owned(), None);
        } else {
            panic!("Can not parse line from ldd: {:?}", line);
        }
    }

    ret
}

fn fs_lint<P: AsRef<Path>>(directory: P) {
    let directory = directory.as_ref();

    xmllint_dir(directory);
    let vars = fs::read_to_string(directory.join("vars.xml")).expect("could not read vars.xml!");

    // TODO: actually parse vars.xml and FreeSWITCH's magic XML
    if !vars.contains("default_odbc_dsn") {
        println!("default_odbc_dsn does not appear to be set in vars.xml");
    }

    if !vars.contains("media_timeout") {
        println!("media_timeout does not appear to be set in vars.xml");
    }

    if !vars.contains("media_hold_timeout") {
        println!("media_hold_timeout does not appear to be set in vars.xml");
    }

    fs_lint_profile(directory.join("sip_profiles/internal.xml"));
    fs_lint_profile(directory.join("sip_profiles/internal-ipv6.xml"));
    fs_lint_profile(directory.join("sip_profiles/external.xml"));
    fs_lint_profile(directory.join("sip_profiles/external-ipv6.xml"));
}

fn fs_lint_profile<P: AsRef<Path>>(file: P) {
    let file = file.as_ref();

    if !file.exists() {
        return;
    }

    let parser = Parser {
        format: ParseFormat::XML,
    };

    let doc = parser.parse_file(file.to_str().unwrap()).unwrap();
    let settings = &doc
        .get_root_element()
        .unwrap()
        .findnodes("settings")
        .unwrap()[0];

    if get_setting(settings, "rtp-timeout-sec") != None {
        println!("deprecated variable rtp-timeout-sec is set in {}, set variable media_timeout in vars.xml instead", file.to_string_lossy());
    }

    if get_setting(settings, "rtp-hold-timeout-sec") != None {
        println!("deprecated variable rtp-hold-timeout-sec is set in {}, set variable media_hold_timeout in vars.xml instead", file.to_string_lossy());
    }
}

fn xmllint_dir<P: AsRef<Path>>(directory: P) {
    let mut all_successful = true;
    for file in WalkDir::new(directory.as_ref())
        .into_iter()
        .map(|entry| entry.unwrap())
        .filter(|entry| entry.file_type().is_file())
        .filter(|entry| entry.path().to_string_lossy().ends_with(".xml"))
    {
        let output = cmd!("xmllint", "--noout", file.path())
            .stderr_to_stdout()
            .stdout_capture()
            .unchecked()
            .run()
            .expect("xmllint could not be ran! is the package libxml2-utils installed?");
        print!("{}", from_utf8(&output.stdout).unwrap());
        if !output.status.success() {
            all_successful = false;
        }
    }

    if all_successful {
        println!("All files pass xmllint validation");
    }
}

fn overwrite_db_setting_for_configuration(filename: &str, name: &str, setting_name: &str) {
    let parser = Parser {
        format: ParseFormat::XML,
    };

    let doc = parser
        .parse_string(fs::read_to_string(filename).unwrap())
        .unwrap();

    let configuration = doc.get_root_element().unwrap();

    assert_eq!(configuration.get_name(), "configuration");
    assert_eq!(configuration.get_property("name").unwrap(), name);

    let settings = &mut configuration.findnodes("settings").unwrap()[0];
    set_setting(settings, &doc, setting_name, Some("$${default_odbc_dsn}"));

    save_doc(&doc, filename);
}

fn overwrite_db_setting_for_profile(filename: &str, name: &str) {
    if !Path::new(filename).exists() {
        return;
    }

    let parser = Parser {
        format: ParseFormat::XML,
    };

    let doc = parser
        .parse_string(fs::read_to_string(filename).unwrap())
        .unwrap_or_else(|_| panic!("could not open {}", filename));

    let configuration = doc.get_root_element().unwrap();

    assert_eq!(configuration.get_name(), "profile");
    assert_eq!(configuration.get_property("name").unwrap(), name);

    let settings = &mut configuration.findnodes("settings").unwrap()[0];
    set_setting(settings, &doc, "odbc-dsn", Some("$${default_odbc_dsn}"));
    set_setting(settings, &doc, "core-db-dsn", None);
    set_setting(settings, &doc, "dbname", None);

    save_doc(&doc, filename);
}

fn update_conference(filename: &str) {
    let parser = Parser {
        format: ParseFormat::XML,
    };

    let doc = parser
        .parse_string(fs::read_to_string(filename).unwrap())
        .unwrap();

    let root = doc.get_root_element().unwrap();
    assert_eq!(root.get_name(), "configuration");
    assert_eq!(root.get_property("name").unwrap(), "conference.conf");

    let sla = &mut root
        .findnodes("/configuration/profiles/profile[@name='sla']")
        .unwrap()[0];
    set_setting(sla, &doc, "rate", Some("auto"));

    save_doc(&doc, filename);
}

fn set_setting(settings: &mut Node, doc: &Document, name: &str, value: Option<&str>) {
    let comment_start = format!(" <param name=\"{}\" value=\"", name);

    let mut was_set = false;
    for mut child in settings.get_child_nodes() {
        if child.get_type() == Some(NodeType::ElementNode)
            && child.get_name() == "param"
            && child.get_property("name") == Some(name.to_string())
        {
            if was_set {
                child.unlink_node();
            } else {
                if let Some(val) = value {
                    child.set_property("value", val).unwrap();
                } else {
                    child.unlink_node();
                }
                was_set = true;
            }
        } else if child.get_type() == Some(NodeType::CommentNode)
            && child.get_content().starts_with(&comment_start)
            && child.get_content().ends_with("\" /> ")
        {
            if was_set {
                child.unlink_node();
            } else {
                if let Some(val) = value {
                    let mut node = Node::new("param", None, doc).unwrap();
                    node.set_property("name", name).unwrap();
                    node.set_property("value", val).unwrap();
                    settings.replace_child_node(node, child).unwrap();
                } else {
                    child.unlink_node();
                }
                was_set = true;
            }
        }
    }

    if !was_set {
        if let Some(val) = value {
            let mut node = Node::new("param", None, doc).unwrap();
            node.set_property("name", name).unwrap();
            node.set_property("value", val).unwrap();
            // TODO: better more automatic formatting
            settings.append_text("  ").unwrap();
            settings.add_child(&mut node).unwrap();
            settings.append_text("\n  ").unwrap();
        }
    }
}

fn get_setting(settings: &Node, name: &str) -> Option<String> {
    for child in settings.get_child_nodes() {
        if child.get_type() == Some(NodeType::ElementNode)
            && child.get_name() == "param"
            && child.get_property("name") == Some(name.to_string())
        {
            return child.get_property("value");
        }
    }
    None
}

fn update_modules(filename: &str) {
    let allowed_modules: HashSet<_> = [
        "mod_amr",
        "mod_b64",
        "mod_cdr_csv",
        "mod_commands",
        "mod_conference",
        "mod_console",
        "mod_db",
        "mod_dialplan_xml",
        "mod_dptools",
        "mod_enum",
        "mod_esf",
        "mod_event_socket",
        "mod_expr",
        "mod_fifo",
        "mod_fsv",
        "mod_g723_1",
        "mod_g729",
        "mod_h26x",
        "mod_hash",
        "mod_httapi",
        "mod_local_stream",
        "mod_logfile",
        "mod_loopback",
        "mod_lua",
        "mod_native_file",
        "mod_opus",
        "mod_png",
        "mod_rtc",
        "mod_say_en",
        "mod_skinny",
        "mod_sms",
        "mod_sndfile",
        "mod_sofia",
        "mod_spandsp",
        "mod_syslog",
        "mod_tone_stream",
        "mod_valet_parking",
        "mod_verto",
        "mod_xml_cdr",
        "mod_xml_rpc",
        "mod_xml_scgi",
    ]
    .iter()
    .cloned()
    .collect();

    let parser = Parser {
        format: ParseFormat::XML,
    };

    let doc = parser.parse_file(filename).unwrap();

    let configuration = doc.get_root_element().unwrap();
    assert_eq!(configuration.get_name(), "configuration");
    assert_eq!(configuration.get_property("name").unwrap(), "modules.conf");

    let modules = &mut configuration.findnodes("modules").unwrap()[0];

    for mut module in modules.get_child_nodes() {
        if module.get_type() == Some(NodeType::ElementNode)
            && module.get_name() == "load"
            && !allowed_modules.contains(&module.get_property("module").unwrap().as_ref())
        {
            println!(
                "Removing unbuilt module {} from modules.conf.xml",
                module.get_property("module").unwrap()
            );
            module.unlink_node();
        }
    }

    save_doc(&doc, filename);
}

fn update_preload_modules<P: AsRef<Path>>(filename: P) {
    fs::write(
        filename,
        r#"<configuration name="pre_load_modules.conf" description="Modules">
  <modules>
    <load module="mod_pgsql"/>
  </modules>
</configuration>
"#,
    )
    .unwrap();
}

fn remove_unused_configs<P: AsRef<Path>>(directory: P) {
    let arr = vec![
        Path::new("autoload_configs/abstraction.conf.xml"),
        Path::new("autoload_configs/alsa.conf.xml"),
        Path::new("autoload_configs/amqp.conf.xml"),
        Path::new("autoload_configs/avmd.conf.xml"),
        Path::new("autoload_configs/blacklist.conf.xml"),
        Path::new("autoload_configs/cdr_csv.conf.xml"),
        Path::new("autoload_configs/cdr_mongodb.conf.xml"),
        Path::new("autoload_configs/cdr_pg_csv.conf.xml"),
        Path::new("autoload_configs/cdr_sqlite.conf.xml"),
        Path::new("autoload_configs/cepstral.conf.xml"),
        Path::new("autoload_configs/cidlookup.conf.xml"),
        Path::new("autoload_configs/dingaling.conf.xml"),
        Path::new("autoload_configs/directory.conf.xml"),
        Path::new("autoload_configs/distributor.conf.xml"),
        Path::new("autoload_configs/easyroute.conf.xml"),
        Path::new("autoload_configs/erlang_event.conf.xml"),
        Path::new("autoload_configs/event_multicast.conf.xml"),
        Path::new("autoload_configs/fax.conf.xml"), // replaced by mod_spandsp
        Path::new("autoload_configs/format_cdr.conf.xml"),
        Path::new("autoload_configs/graylog2.conf.xml"),
        Path::new("autoload_configs/hiredis.conf.xml"),
        Path::new("autoload_configs/http_cache.conf.xml"),
        Path::new("autoload_configs/java.conf.xml"),
        Path::new("autoload_configs/kazoo.conf.xml"),
        Path::new("autoload_configs/lcr.conf.xml"),
        Path::new("autoload_configs/memcache.conf.xml"),
        Path::new("autoload_configs/mongo.conf.xml"),
        Path::new("autoload_configs/nibblebill.conf.xml"),
        Path::new("autoload_configs/opal.conf.xml"),
        Path::new("autoload_configs/oreka.conf.xml"),
        Path::new("autoload_configs/osp.conf.xml"),
        Path::new("autoload_configs/perl.conf.xml"),
        Path::new("autoload_configs/pocketsphinx.conf.xml"),
        Path::new("autoload_configs/portaudio.conf.xml"),
        Path::new("autoload_configs/python.conf.xml"),
        Path::new("autoload_configs/redis.conf.xml"),
        Path::new("autoload_configs/rss.conf.xml"),
        Path::new("autoload_configs/sangoma_codec.conf.xml"),
        Path::new("autoload_configs/shout.conf.xml"),
        Path::new("autoload_configs/smpp.conf.xml"),
        Path::new("autoload_configs/sms_flowroute.conf.xml"),
        Path::new("autoload_configs/spidermonkey.conf.xml"),
        Path::new("autoload_configs/translate.conf.xml"), // interesting
        Path::new("autoload_configs/tts_commandline.conf.xml"),
        Path::new("autoload_configs/unimrcp.conf.xml"),
        Path::new("autoload_configs/v8.conf.xml"),
        Path::new("autoload_configs/voicemail.conf.xml"),
        Path::new("autoload_configs/voicemail_ivr.conf.xml"),
        Path::new("autoload_configs/xml_curl.conf.xml"),
        Path::new("autoload_configs/zeroconf.conf.xml"),
    ];

    for relative_file in &arr {
        let file = directory.as_ref().join(relative_file);
        if file.exists() {
            println!("Deleting unused config file {}", file.to_string_lossy());
            fs::remove_file(file).unwrap();
        }
    }

    let mrcp_profiles = directory.as_ref().join("mrcp_profiles");
    if mrcp_profiles.exists() {
        println!("removing directory: {}", mrcp_profiles.to_string_lossy());
        fs::remove_dir_all(mrcp_profiles).unwrap();
    }

    for file in WalkDir::new(directory)
        .into_iter()
        .map(|entry| entry.unwrap())
        .filter(|entry| entry.file_type().is_file())
        .filter(|entry| entry.path().extension() == Some(OsStr::new("xml~")))
    {
        println!(
            "Removing editor temporary file {}",
            file.path().to_string_lossy()
        );
        fs::remove_file(file.path()).unwrap();
    }
}

fn get_installed_packages() -> HashSet<String> {
    let selections = cmd!("dpkg", "--get-selections")
        .read()
        .expect("could not run dpkg");

    let mut ret: HashSet<String> = HashSet::new();

    for line in selections.lines() {
        let split: Vec<&str> = line.split_whitespace().collect();

        if split.len() == 2 {
            if split[1] == "install" {
                ret.insert(split[0].split(':').next().unwrap().to_owned());
            }
        } else {
            panic!("dpkg returned malformed line: {}", line);
        }
    }

    ret
}

// TODO: take configuration directory, use modules.conf and pre_load_modules.conf in that directory
// aka only check packages for loaded modules
fn check_fs_packages() {
    let installed = get_installed_packages();

    // TODO: figure this out automatically
    // run `readelf -d file | grep NEEDED` and then `dpkg -S file.so` on everything (`dpkg -S` only works on installed packages though)
    // TODO: keep track of dependencies per module, and only warn if a mod is being loaded and its dependencies aren't available
    let core = vec![
        // core FreeSWITCH
        "libcurl3",
        "libspeex1",
        "libspeexdsp1",
        "libssl1.1",
        // libfreeswitch.so
        "libsqlite3-0",
        "libpcre3",
        "zlib1g",
        "libstdc++6",
        "libgcc1",
        // mod_enum
        "libldns2",
        // mod_lua
        "liblua5.2-0",
        // mod_opus
        "libopus0",
        // mod_pgsql (in pre_load_modules)
        "libpq5",
        // mod_sndfile
        "libsndfile1",
        // mod_spandsp
        "libjbig0",
        "liblzma5",
        "libjpeg62-turbo",
        "libtiff5",
    ];

    let mut to_install = Vec::new();

    for p in core {
        if !installed.contains(p) {
            to_install.push(p);
        }
    }

    if !to_install.is_empty() {
        println!(
            "Packages are missing, please run `sudo apt install {}` to install missing packages",
            to_install.join(" ")
        );
    }
}

fn save_doc<P: AsRef<Path>>(doc: &Document, filename: P) {
    let doc = doc.dup().unwrap();

    let string = doc.to_string_with_options(SaveOptions {
        format: true,
        no_declaration: true,
        ..Default::default()
    });
    fs::File::create(filename)
        .unwrap()
        .write_all(string.as_bytes())
        .unwrap();
}

lazy_static! {
    static ref RECORDINGS: HashMap<&'static Path, &'static [u8]> = [
        (
            Path::new("Beep1-mitel-10sec.wav"),
            include_bytes!("../custom_recordings/Beep1-mitel-10sec.wav") as &[u8]
        ),
        (
            Path::new("No_attended_transfer.wav"),
            include_bytes!("../custom_recordings/No_attended_transfer.wav")
        ),
        (
            Path::new("not_in_service.wav"),
            include_bytes!("../custom_recordings/not_in_service.wav")
        )
    ]
    .iter()
    .cloned()
    .collect();
}

fn update_custom_recordings<P: AsRef<Path>>(dir: P) {
    let dir = dir.as_ref();

    fs::create_dir_all(dir)
        .unwrap_or_else(|_| panic!("could not create directory {}", dir.to_string_lossy()));

    for (&name, &content) in &*RECORDINGS {
        let file = dir.join(name);
        if file.exists() {
            let orig_contents = fs::read(&file)
                .unwrap_or_else(|_| panic!("could not read {}", file.to_string_lossy()));
            if orig_contents != content {
                println!(
                    "{}",
                    Colour::Red.paint(format!(
                        "{} is not as expected, delete and rerun to replace",
                        file.to_string_lossy()
                    ))
                );
            }
        } else {
            fs::write(&file, content)
                .unwrap_or_else(|_| panic!("could not write {}", file.to_string_lossy()));
            println!("writing recording {}", file.to_string_lossy());
        }
    }
}

lazy_static! {
    static ref CONFIGS: HashMap<&'static Path, &'static str> = [
        (
            Path::new("dialplan/controller_blind_transfer_context.xml"),
            include_str!("../configs/controller_blind_transfer_context.xml")
        ),
        (
            Path::new("dialplan/00_EavesDrop.xml"),
            include_str!("../configs/00_EavesDrop.xml")
        ),
        (
            Path::new("autoload_configs/conference.conf.xml"),
            include_str!("../configs/conference.conf.xml")
        ),
        (
            Path::new("autoload_configs/event_socket.conf.xml"),
            include_str!("../configs/event_socket.conf.xml")
        )
    ]
    .iter()
    .cloned()
    .collect();
}

fn overwrite_fs_configs<P: AsRef<Path>>(dir: P) {
    let dir = dir.as_ref();

    for (&name, &content) in &*CONFIGS {
        let file = dir.join(name);
        if file.exists() {
            let orig_contents = fs::read_to_string(&file)
                .unwrap_or_else(|_| panic!("could not read {}", file.to_string_lossy()));
            if orig_contents != content {
                fs::write(&file, content)
                    .unwrap_or_else(|_| panic!("could not write {}", file.to_string_lossy()));
                println!("overwriting config {}", file.to_string_lossy());
            }
        } else {
            fs::write(&file, content)
                .unwrap_or_else(|_| panic!("could not write {}", file.to_string_lossy()));
            println!("creating config {}", file.to_string_lossy());
        }
    }

    update_internal_sip_profile(dir.join("sip_profiles/internal.xml"));
}

fn update_internal_sip_profile<P: AsRef<Path>>(file: P) {
    let file = file.as_ref();

    let parser = Parser {
        format: ParseFormat::XML,
    };
    let doc = parser
        .parse_file(file.to_str().unwrap())
        .unwrap_or_else(|_| panic!("could not parse file {}", &file.to_string_lossy()));

    let settings = &mut doc
        .get_root_element()
        .unwrap()
        .findnodes("settings")
        .unwrap()[0];
    set_setting(settings, &doc, "manage-presense", Some("true"));
    set_setting(settings, &doc, "manage-shared-appearance", Some("true"));
    set_setting(settings, &doc, "inbound-codec-negotiation", Some("greedy"));

    save_doc(&doc, file);
}
